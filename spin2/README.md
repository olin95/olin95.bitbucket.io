# Wheel of Fortune

### Add code
file: editable/code.csv

x = number of rotation

d = degree of angle

target = (x*360)+x

**format**
- (code),0,(target+1),(target-1),(target),(reward)

---
### Change spinner size

file: editable/spinner.css

line 5

---
### Change spinner speed

file: editable/spinAround.css

line 6 to 9

all values must be same

**simulator:**

http://cubic-bezier.com/#.17,.67,.64,.98

---
### Change spinner image

file: PROMOTION.png, POINTER.png

https://gitforwindows.org

cd c:/

git clone **

cd ""

replace image

git add *

git commit -m "image version X"

git push

---
## Developer's note
- Do not use jQuery in bitbucket as jQuery need security code
- jQuery will cause https to block all js from bitbucket

### Port to server

index.html line 36 to 59

src/javascript.js

	- createModal will make the wheel spin and prepare for generating the popUp
	- popUp will generate content of popUp

Updated on 5th August 2018