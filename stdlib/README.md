# stdlib
### popup
- to create a popup box
### undeConstruction
- a css stamp with the word "under construction"
### comparing.js
- comaparing 2 list
### create_items.js
- create table cell
- create checkbox
- create ul list
### csv_array.js
- convert strings from csv to array
### get_values.js
- retrives values from radio button by name
### toggle.js
- changes class name by id
- changes class name by class name (the indicator class names must no be the same as the class to change)
### time.js
- converts time interval to a proper format
- check is the input in between a duration
### table_function.js
- sorts table by a column
- sorts table by a class name