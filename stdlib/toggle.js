function toggle_elements_class(class_name, class_array) {
    var element_list = document.getElementsByClassName(class_name);
    for (var i = 0; i < element_list.length; i++) {
        element_list[i].className = element_list[i].className.replace(class_array[0], class_array[1]);
    }
}

function toggle_by_id(id, toggle_array) {
	var element = document.getElementById(id);
	element.className = element.className.replace(toggle_array[0], toggle_array[1]);
}