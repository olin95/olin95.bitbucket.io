function radio_value(name) {
	var radios = document.getElementsByName(name);
	var return_val = null;
	for (var i = 0; i < radios.length; i++)
		if (radios[i].checked == true){
			return_val = radios[i].value;
			break;
		}
	return return_val;
}
function processList(list_tag, className, splitter, indicator_index, combinator, grouping){
	var values = {};
	var list = list_tag.getElementsByClassName(className);
	for (var i = 0; i < list.length; i++) {
		var key_name = list[i].id.split(splitter)[indicator_index];
		if (key_name in values)
			values[key_name] = values[key_name] + combinator + list[i].innerHTML;
		else
			values[key_name] = list[i].innerHTML;
	}
	if (grouping.length > 0) {
		var keys = [grouping[0]];
		var const_values = [values[grouping[0]]];
		for (var i = 1; i < grouping.length; i++) {
			if (values[grouping[i - 1]] == values[grouping[i]])
				keys[keys.length - 1] = keys[keys.length - 1] + combinator + grouping[i];
			else {
				keys.push(grouping[i]);
				const_values.push(values[grouping[i]]);
			}
		}
		if (const_values.length > 1 && const_values[0] == const_values[const_values.length - 1]) {
			keys[0] = keys[const_values.length - 1] + combinator + keys[0];
			keys.pop();
			const_values.pop();
		}
		values = {};
		for (var i = 0; i < keys.length; i++) {
			values[keys[i]] = const_values[i];
		}
	}
	return values;
}