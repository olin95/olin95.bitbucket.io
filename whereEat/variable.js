var category_list = {
	"L": "Light Meal",
	"H": "Heavy Meal",
	"S": "Supper",
	"T": "Tea",
};
var current_cost = 3;
var current_day = "";
var d = new Date();
var days_name = {
	0:"sunday",
	1:"monday",
	2:"tuesday",
	3:"wednesday",
	4:"thursday",
	5:"friday",
	6:"saturday",
};
var on_off = {
	0: 'X',
	1: '/',
};
var shop_csv = "whereEat-shops.csv";
var shopType = {
	"MK": "Mamak",
	"MR": "Mix Rice",
	"RT": "Restaurant",
	"OT": "Other",
	"BS": "Bistro",
};
var table_headers = [
	'include',
	'shop_name',
	'type',
	'working_days',
	'working_hours',
	'cost',
	'category',
	'desc',
	'locations',
];
var locations = {
	"KJ": "Kajang",
	"SL": "Sg Long",
	"MC": "Mahkota",
};