create_checkboxes(category_list, "category_checkboxes", "category_checkboxes_container", "change", "category", "category_cb", "category_label", true);
create_checkboxes(shopType, "shop_type_checkboxes", "shop_type_checkboxes_container", "change", "filter", "type_filter", "type_filter_label", true);
create_checkboxes(locations, "locations_checkboxes", "locations_checkboxes_container", "change", "locations", "locations_cb", "locations_label", true);

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var tableBody = document.getElementById("shop_table_tbody");
        var shopObjects = csvToArray(this.responseText);
        for (var shop in shopObjects)
            createTableRow(tableBody, shopObjects[shop], shop);
    }
};
xhttp.open("GET", "../databases/" + shop_csv, true);
xhttp.send();

for (var filter in shopType)
    document.getElementById('filter|' + filter).checked = true;
var hour;
var minute;
if (d.getHours().toString().length < 2)
    hour = "0" + d.getHours();
else
    hour = d.getHours();
if (d.getMinutes().toString().length < 2)
    minute = "0" + d.getMinutes();
else
    minute = d.getMinutes();
document.getElementById('time_input').value = hour + ":" + minute;